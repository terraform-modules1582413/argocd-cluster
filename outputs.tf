output "cluster_destination" {
  value = var.argocd_cluster_endpoint
}

output "cluster_name" {
  value = var.argocd_cluster_name
}
