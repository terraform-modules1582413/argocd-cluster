resource "google_project_iam_member" "argocd_iam_sa_roles" {

  role    = "roles/container.admin"
  member  = "serviceAccount:${var.argocd_sa_email}"
  project = var.gcp_project_id
}

resource "kubernetes_service_account" "argocd_sa" {
  metadata {
    name      = "argocd"
    namespace = "kube-system"
    annotations = {
      "iam.gke.io/gcp-service-account" = var.argocd_sa_email
    }
  }
}

resource "kubernetes_cluster_role_binding" "argocd_role" {
  metadata {
    name = "argocd"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }
  subject {
    kind = "ServiceAccount"
    name = "argocd"
  }
}

resource "kubernetes_secret" "argocd_cluster" {
  provider = kubernetes.argocd-gke

  metadata {
    name      = var.argocd_cluster_name
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "cluster"
    }
  }
  data = {
    name   = var.argocd_cluster_name
    server = var.argocd_cluster_endpoint
    config = templatefile("${path.module}/config.json", {
      ca_cert = var.argocd_cluster_ca_cert
    })
  }
  type = "Opaque"
}
