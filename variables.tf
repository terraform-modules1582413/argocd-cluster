variable "gcp_project_id" {
  type = string
}

variable "argocd_cluster_ca_cert" {
  type = string
}
variable "argocd_cluster_endpoint" {
  type = string
}
variable "argocd_cluster_name" {
  type = string
}
variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_sa_email" {
  type = string
}
