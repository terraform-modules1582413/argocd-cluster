<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.18.1 |
| <a name="provider_kubernetes.argocd-gke"></a> [kubernetes.argocd-gke](#provider\_kubernetes.argocd-gke) | ~> 2.18.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [google_project_iam_member.argocd_iam_sa_roles](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [kubernetes_cluster_role_binding.argocd_role](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/cluster_role_binding) | resource |
| [kubernetes_secret.argocd_cluster](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_service_account.argocd_sa](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service_account) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argocd_cluster_ca_cert"></a> [argocd\_cluster\_ca\_cert](#input\_argocd\_cluster\_ca\_cert) | n/a | `string` | n/a | yes |
| <a name="input_argocd_cluster_endpoint"></a> [argocd\_cluster\_endpoint](#input\_argocd\_cluster\_endpoint) | n/a | `string` | n/a | yes |
| <a name="input_argocd_cluster_name"></a> [argocd\_cluster\_name](#input\_argocd\_cluster\_name) | n/a | `string` | n/a | yes |
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_sa_email"></a> [argocd\_sa\_email](#input\_argocd\_sa\_email) | n/a | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_destination"></a> [cluster\_destination](#output\_cluster\_destination) | n/a |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | n/a |
<!-- END_TF_DOCS -->