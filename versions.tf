terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.18.1"
    }
    google = {
      source  = "hashicorp/google"
      version = "~> 4.59.0"
    }
  }
}
